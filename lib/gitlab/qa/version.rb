# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '14.5.0'
  end
end
