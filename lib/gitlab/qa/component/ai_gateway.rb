# frozen_string_literal: true

module Gitlab
  module QA
    module Component
      class AiGateway < Base
        DOCKER_IMAGE = 'registry.gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist/model-gateway'
        DOCKER_IMAGE_TAG = 'latest'

        def name
          @name ||= 'ai-gateway'
        end

        def configure_environment(gitlab_hostname:)
          @environment = {
            'AIGW_GITLAB_URL' => "http://#{gitlab_hostname}",
            'AIGW_GITLAB_API_URL' => "http://#{gitlab_hostname}/api/v4",
            'AIGW_CUSTOMER_PORTAL_URL' => Runtime::Env.customer_portal_url,
            'AIGW_USE_FAKE_MODELS' => true
          }
        end
      end
    end
  end
end
